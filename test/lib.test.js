import Debug from 'debug'
import { describe } from 'riteway'
const debug = Debug('test')

let {ChannelFactory: ChannelSocketIOServer } = require('../index.js')
import {ChannelFactory as ChannelSocketIOClient} from 'real-value-channel-socketio-client-buffered'

describe.only('Channel', async (assert) => {

  var app = require('express')();
  var server = require('http').Server(app);
  server.listen(9000,()=>{debug('Listening')});

  let channelServer = ChannelSocketIOServer({server})('production',{bufferLength: 2})

  let channelClient = ChannelSocketIOClient({url: "http://127.0.0.1:9000"})('production')
  
  channelServer.enqueue({ fromServer: '1'})
  channelClient.enqueue({ fromClient: '2'})
  
  channelServer.enqueue({ fromServer: '3'})
  channelClient.enqueue({ fromClient: '4'})
  
  channelServer.enqueue({ fromServer: '5'})
  channelClient.enqueue({ fromClient: '6'})

  await new Promise((resolve)=>{ setTimeout(resolve,3000)})

  let clientItem = null
  if(channelServer.length()>0){
    clientItem= channelServer.dequeue()
  }
  if(channelServer.length()>0){
    clientItem= channelServer.dequeue()
  }

  let serverItem = null
  if(channelClient.length()>0){
    serverItem = channelClient.dequeue()
  }
  if(channelClient.length()>0){
    serverItem = channelClient.dequeue()
  }

  assert({
    given: 'server content sent',
    should: 'client1 content received',
    actual: `${clientItem.fromClient} ${serverItem.fromServer}`,
    expected: '4 5'
  })

  channelClient.close()

  let channelClient2 = ChannelSocketIOClient({url: "http://127.0.0.1:9000"})('production')

  if(channelClient2.length()>0){
    serverItem = channelClient2.dequeue()
  }
  if(channelClient2.length()>0){
    serverItem = channelClient2.dequeue()
  }

  assert({
    given: 'server content sent',
    should: 'client2 content received',
    actual: `${serverItem.fromServer}`,
    expected: '5'
  })
  
  channelClient2.close()

  server.close(()=>{})  
})

describe('Channel:Error on Dequeue When no content', async (assert) => {

  var app = require('express')();
  var server = require('http').Server(app);
  server.listen(9000,()=>{debug('Listening')});

  let channelServer = ChannelSocketIOServer({server})('production')

  try {
    let answer = channelServer.dequeue()
  } catch(err){
    assert({
      given: 'dequeue when length is zero',
      should: 'receive error',
      actual: err.message,
      expected: 'Ensure length is > 0 before invoking dequeue'
    })  
  }

  server.close(()=>{
  })
  
})

