# real-value-channel-socketio-server-buffered Library     

## About

This library provides a server side implementation of a channel.

A channel is an object once established which provides 3 methods.
- the `enqueue` method to push content into the channel
- the `length` method to check if there is content to retreive from the channel
- the `dequeue` method to get unseen content from the channel

The channel keeps a buffer of length `bufferLength` such that new clients receive the buffered content.
This provides that value in that the server can retain some historical data so that when new channel clients connect they get the historical data and any subsequent new data that arrives.

## Install

```
npm install real-value-channel-socketio-server-buffered
yarn install real-value-channel-socketio-server-buffered
```

## How to use

Instantiate the channel
```
var app = require('express')();
var server = require('http').Server(app);
server.listen(9000,()=>{debug('Listening')});

let ChannelFactory = ChannelSocketIOServer({server})
let channel = ChannelFactory('production',{bufferLength: 2})  //A channel of production data with a buffer length of 2
```

Then use the `enqueue`, `length`, `dequeue` methods as required