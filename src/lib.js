import assert from 'assert'
import Debug from 'debug'
const debug = Debug('lib-real-value-channel-socketio-server-buffered')

import IO from 'socket.io'

function Stream(name,bufferLength){
  let streamName = name
  let streamLog = []
  let streamNextId = 0
  
  const push = (x)=>{
    streamLog.push(x)
    streamNextId++
    debug(`Stream ${streamName} pushed ${x}`)
    //maybe need to shift here

    if(streamLog.length>bufferLength){
      debug('Dropping content')
      streamLog.shift()
      debug(`Log length: ${streamLog.length}`)
    }
  }

  const max = ()=>streamNextId

  const get = (offset)=>{
    debug(`get ${offset}`)

    let logOffset
    let nextOffset = offset+1
    if(offset===-1){
      logOffset =   0
      nextOffset = streamNextId - (streamLog.length)+1
    }else {
      logOffset = streamLog.length - (streamNextId-offset)
    }
    return { nextItem: nextOffset,value:streamLog[logOffset]}
  }

  return {
    push,
    max,
    get
  }
}

function Consumer(socket,streams){
  debug(socket.id)
  let consumerSocket = socket

  let disconnected = false
  let subscriptions = {}

  const subscribe = (streamName)=>{
    debug(`Consumer ${consumerSocket.id} subscribing to ${streamName}`)
    subscriptions[streamName]=-1
  }

  const start = ()=>{
    startSending()
  }

  const stop =()=>{disconnected=true}

  function startSending(){
    debug('Start Sending')
    Object.keys(subscriptions).forEach((streamName)=>{
      debug(`Processing Subscription ${streamName}`)
      let nextRequired = subscriptions[streamName]
      if(streams[streamName]){
        let stream = streams[streamName]
        while(nextRequired<stream.max()){
          let { nextItem ,value} = stream.get(nextRequired)
          debug(`Emitting value ${value} on stream:${streamName}`)
          consumerSocket.emit('Channel',value)
          debug(`Next value required ${nextItem}`)
          subscriptions[streamName] = nextRequired = nextItem
        }
      }
    }) 
    if(!disconnected){
        setTimeout(()=>{
          startSending()
        },500)
    }else {
        debug('Disconnected: Stop sending')
    }
  }

  return {
    subscribe,
    start,
    stop
  }
}

export function ChannelFactory (options) {

  let { server } = options

  let streams = {}
  let consumers = {}

  let inBuffer = {}

  var io = IO(server);

  io.on('connection', function(socket){
      debug('a user connected')
    
      let consumer = Consumer(socket,streams)
      consumer.start()
      
      socket.on('Channel',(msg)=>{
          if(msg.channel==='subscribe'){
            debug(`Server subscribe consumer to channel: ${msg.value}`)
            consumer.subscribe(msg.value)
            debug('subscribed')
          }else {
            debug(`Server received on channel: ${msg.channel}`)
            if(inBuffer[msg.channel]===undefined){
              inBuffer[msg.channel]=[]
          }
          inBuffer[msg.channel].push(msg.value)
          }

      })

      socket.on('disconnect', ()=>{
          debug('user disconnected')
          consumer.stop()
      })
      
  })

  return (name,options={})=> {
    const {bufferLength=100}=options
      return {
          enqueue: x => {
              debug(`enqueue ${name}`)
              if(!streams[name]){
                  streams[name]= Stream(name,bufferLength)
              }
              const stream = streams[name]
              stream.push({channel: name, value: x})
          },
          length: () => inBuffer[name]!==undefined ? inBuffer[name].length : 0,
          dequeue: () => {
              assert(inBuffer[name] && inBuffer[name].length>0,"Ensure length is > 0 before invoking dequeue")
              let ans = inBuffer[name].shift()
              debug(`dequeue ${name} buffer length:${inBuffer[name].length}: ${JSON.stringify(ans)}`)
              return ans
          }
      }
  }
}